import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf

tf.compat.v1.disable_eager_execution()

# Placeholder
d1 = tf.compat.v1.placeholder(dtype=tf.int32, shape=(1, 3), name='d1') # dạng một vctor hàng
d2 = tf.compat.v1.placeholder(dtype=tf.int32, shape=(1), name='d2') # dạng 1 số
d3 = tf.compat.v1.placeholder(dtype=tf.int32, shape=(1), name='d3') # dạng 1 số
d4 = tf.compat.v1.placeholder(dtype=tf.int32, shape=(2, 3), name='d4') # dạng 1 ma trận cỡ 2 x 3
d5 = tf.compat.v1.placeholder(dtype=tf.int32, shape=(3, 1), name='d5') # dạng 1 ma trận cỡ 3 x 1
a = tf.constant([[1, 2, 3]], dtype=tf.int32, name='a') # hằng là vector

c = tf.add(a, d1)
d = tf.add(d2, d3)
e = tf.matmul(d4, d5) # phép nhân 2 ma trận
# f = tf.multiply(d4, d5) # sẽ báo lỗi vì chỉ nhân khi giống dim hoặc nhân 1 vector, ma trận, tensor với 1 số
# Tạo 1 session
sess = tf.compat.v1.Session()

with sess:
    print('c = ' + str(sess.run(c, feed_dict={d1:[[100, 100, 100]]})))
    print('-' * 20)
    print('e = ' + str(sess.run(e, {d4:[[1, 2, 3], [1, 2, 3]], d5:[[1], [2], [3]]})))
    print('-' * 20)
    print('d = ' + str(sess.run(d, feed_dict={d2:[1], d3:[3]})))
    print('-' * 20)

sess.close()

# Tạo 1 graph
writer = tf.compat.v1.summary.FileWriter('./second_graph', tf.compat.v1.get_default_graph())
writer.close()