import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt

tf.compat.v1.disable_eager_execution()

X = tf.compat.v1.placeholder(name='X', dtype=tf.float32) # không cần khai báo shape
Y = tf.compat.v1.placeholder(name='Y', dtype=tf.float32) # không cần khai báo shape

# Dữ liệu
x_data = [3.3, 4.4, 5.5, 6.71, 6.93, 4.168, 9.779, 6.182, 7.59, 2.167, 7.042, 10.791, 5.313, 7.997, 5.654, 9.27, 3.1]
y_data = [1.7, 2.76, 2.09, 3.19, 1.694, 1.573, 3.366, 2.596, 2.53, 1.221, 2.827, 3.465, 1.65, 2.904, 2.42, 2.94, 1.3]

# Đố thị
plt.xlabel('X')
plt.ylabel('Y')
plt.plot(x_data, y_data, 'ro')
plt.show()

# Xây dựng bias và weight
w = tf.compat.v1.get_variable('weight', initializer=tf.constant(0.0))
b = tf.compat.v1.get_variable('bias', initializer=tf.constant(0.0))

# Hàm loss và cách tối ưu
Yhat = w * X + b
loss = tf.square(Y - Yhat, name='loss')
optimize = tf.compat.v1.train.GradientDescentOptimizer(learning_rate=0.001).minimize(loss)

# Viết graph
writer = tf.compat.v1.summary.FileWriter('./third_graph', tf.compat.v1.get_default_graph())

sess = tf.compat.v1.Session()

with sess:
    sess.run(tf.compat.v1.global_variables_initializer())
    for epoch in range(100):
        total_loss = 0
        for pos in range(len(x_data)):
            _, _loss = sess.run([optimize, loss], feed_dict={X:x_data[pos], Y:y_data[pos]})
            total_loss += _loss
        print('Epoch {}, loss: {}'.format(epoch, total_loss / len(x_data)))
    w_out, b_out = sess.run([w, b])

w_out = float(w_out)
b_out = float(b_out)

Yhat = list(np.array(x_data) * w_out + b_out)

plt.xlabel('X')
plt.ylabel('Y')
plt.plot(x_data, Yhat)
plt.plot(x_data, y_data, 'ro')
plt.show()



