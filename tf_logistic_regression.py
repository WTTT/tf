import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.regularizers import L1L2

x_train = np.array([[1],[2],[3],[4]])  
y_train = np.array([[1],[0],[1],[0]])  
x_val = np.array([[5],[7],[8]])    
y_val = np.array([[1],[1],[0]])   

model = Sequential()
model.add(Dense(2,  activation='softmax', kernel_regularizer=L1L2(l1=0.0, l2=0.1), input_dim=1))  
model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
model.fit(x_train, y_train, epochs=100, validation_data=(x_val, y_val))


