import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf

tf.compat.v1.disable_eager_execution()

# Tạo graph
graph = tf.compat.v1.get_default_graph()

# Hằng trong tf
a = tf.constant(3, name='a')
b = tf.constant(5, name='b')

# Phép tính
c = tf.add(a, b, name='c')
d = tf.subtract(a, b, name='d')
e = tf.divide(a, b, name='e')
f = tf.multiply(a, b, name ='f') # nhân thông thường
g = tf.add(3, 5, name='g')
h = tf.add(tf.add(tf.add(c, d), g), f, name='h')
i = tf.divide(a, g, name = 'i')
k = tf.add(e, i, name='k')

# Biến
x = tf.Variable(3, name='X') # chưa khởi tạo
y = tf.Variable(5, name='Y') # chưa khởi tạo

z = tf.compat.v1.get_variable(name='z', initializer=tf.constant(39)) # biến được khởi tạo ngay
t = tf.add(z, x, name='t')

# Viết và đóng Graph
writer = tf.compat.v1.summary.FileWriter("./first_graph", graph)

tf.compat.v1.global_variables_initializer() # khởi tạo toàn bộ biến

writer.close()

# Tạo session
sess = tf.compat.v1.Session()

with sess:
    sess.run(tf.compat.v1.global_variables_initializer()) # khởi tạo toàn bộ biến trong session
    print('Constant:')
    print('a: ' + str(sess.run(a)))
    print('b: ' + str(sess.run(b)))
    print('c: ' + str(sess.run(c)))
    print('d: ' + str(sess.run(d)))
    print('e: ' + str(sess.run(e)))
    print('f: ' + str(sess.run(f)))
    print('g: ' + str(sess.run(g)))
    print('h: ' + str(sess.run(h)))
    print('i: ' + str(sess.run(i)))
    print('k: ' + str(sess.run(k)))
    print('-' * 20)
    print('Variable:')
    print('x: ' + str(sess.run(x)))
    print('y: ' + str(sess.run(y)))
    print('z: ' + str(sess.run(z)))
    print('-' * 20)
    print('Result of z + y')
    print('t: ' + str(sess.run(t)))

sess.close()
